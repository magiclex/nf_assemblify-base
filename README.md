![License](https://img.shields.io/badge/License%20-%20CC0%201.0%20Universal%20-%20red?style=flat-square&logoColor=black
)
![Contributors](https://img.shields.io/badge/Contributors%20-%202%20-%20black?style=flat-square&logoColor=black
)

# Assemblify-base Workflow

## Introduction

Assemblify-base is a modular worklow pipeline based on Singularity dependencies and Nextflow pipeline language. Since the workflow uses singularity containers no complex dependencies tools need to be installed.

It is a workflow that is aimed for genome assembly and annotation. Assembly-base is derived from a step-by-step analysis carried by Ifremer institute on the Dunaliella primolecta genome.

It is data collected from Illumina and PacBio sequencers of paired-end reads.

Since it is a modular workflow, each step of the pipeline is isolated within a dedicated codes available in bin and module folders. Hence, it is possible to adapt this workflow depending on the needs of your study by removing, replacing or adding new modules.

## Assembly-base architecture

`./conf` : Directory containing the configuration files of the workflow.

`./conf/resources.config` : File containing RAM/CPU/TIME to be used with the tools.

`./conf/custom.config`: File containing the different variables adjustable for the pipeline.

`./bin` : Directory containing the shell scripts executing tools.

`./module` : Directory contains the Nexflow steps(.nf files) of the overall pipeline.

`./scripts` : Directory conatining .pbs scripts.

`./main.nf` : File describing the chaining of modules (It is the workflow itself).

`./nextflow.config` : File setting up the Singularity images to use during pipeline execution.

`./ifb-core.config` : File containing the resources definition to execute the pipeline on IFB Core Cluster.

`./run_assemblify.slurm` : File to execute to submit the pipeline execution on a cluster.

## Pipeline overview
![Pipeline](https://gitlab.com/magiclex/nf_assemblify-base/-/raw/master/img/workflow_PLA.png?ref_type=heads)

### 1. Check the quality of the reads

Quality control with Nanoplot for the PacBio Hifi data or FastQc + MultiQc for Illumina data.

#### **a) NanoPlot**

`bin/nanoplot.sh` shell script that is called in the  `modules/nanoplot.nf`  module needed in the workflow.

Version of the Singularity image : _**nanoplot:1.32.1--py_0**_

**Command line:**
```{bash}
NanoPlot -t ${NCPUS} --plots kde dot hex --N50 --title \"PacBio Hifi reads for $(basename ${FASTQ_FILE%.hifi*})\" --fastq ${FASTQ_FILE} -o .
```

**Arguments:**

The arguments selected to executed this step of the pipeline are: 
- `-t` : Number of CPUs to be used for processing. 
- `--plots` : Specifies the types of plots to generate.
- `--N50` : N50 statistics.
- `--title` : Sets the title for the plots.
- `--fastq`:  Specifies the input FASTQ file containing PacBio HiFi reads.
- `-o`: Sets the output directory for the generated plots.

**Variables**

- NCPUS: Numbers of CPU
- FASTQ_FILE: Sequencing files (fastq.gz)

#### **b) FastQC**

`bin/fastqc.sh` shell script that is called in the `modules/fastqc.nf` module needed in the workflow.

Version of the Singularity image : _**fastqc:0.11.9--hdfd78af_1**_

**Command line:**

```{bash}
fastqc ${FASTQ_FILE} -o .
```

**Arguments:**

- `-o`: Sets the output directory for the generated plots

**Variables:**

- FASTQ_FILE: Sequencing files(fastq.gz)

#### **c) MultiQc**

Files: `bin/multiqc.sh`shell script that is called in the `modules/multiqc.nf`  module needed in the workflow.

Version of the Singularity image : _**multiqc:1.14--pyhdfd78af_0**_

**Command line:**

```{bash}
multiqc --outdir . --filename $REPORT_NAME ${DIR}
```

**Arguments:**

- `--outdir`: Specifies the output directory for the MultiQC report.
- `--filename`:  Specifies the filename for the MultiQC report.

**Variables:**

- DIR: Output directory

### 2. Contiging / Assembly: Hifiasm

Assemblies are done with Hifiasm.

`bin/hifiasm.sh` shell script that is called in the  `modules/hifiasm.nf` module needed in the workflow.

Version of the Singularity image : _**hifiasm:0.18.9--h5b5514e_0**_

**Command line:**

```{bash}
hifiasm -k 51 -t ${NCPUS} ${HIFI}
```
**Arguments:**

- `-k`: Specifies the k-mer size for the assembler. 
- `-t`: Number of CPUs to be used for processing. 

**Variables:** 

- NCPUS: Numbers of CPU
- HIFI: HiFi PacBio data files(fast.gz)

### 3. Quality Control: BUSCO

**Busco** is used to check the quality of our contig(s).

`bin/busco.sh` shell script that is called in the `modules/busco.nf` module needed in the workflow.

Version of the Singularity image : _**busco:5.5.0--pyhdfd78af_0**_

**Command line:**

```{bash}
busco -c ${NCPUS} --offline -m genome -i ${ASSEMBLY_FA} -o res.busco -l ${DBPATH}/${DBNAME}
```
**Arguments:**

- `-c`: Specifies the number of CPU.
- `--offline`: Runs BUSCO in offline mode, meaning it uses a precomputed database without internet access.
- `-m`: Specifies the mode for assessing genome completeness.
- `-i`: Specifies the input genome assembly file.
- `-o`: Specifies the output directory.
- `-l`: Specifies the location of the BUSCO database.

**Variables:**

- NCPUS: Number of CPU
- ASSEMBLY_FA: Hifiasm output
- DBPATH: Output Directory
- DBNAME: chlorophyta_odb10

Note: Because of issues, this process was commented. It should be un-commented (by removing the two backslash) to make it work.

### 4. RepeatMasking: RED

Predicting repeat elements on genomic sequences are done with **Red**.

`bin/redmask.sh`  shell script that is called in the `modules/redmask.nf` module needed in the workflow.

Version of the Singularity image : red:2018.09.10--h4ac6f70_2

**Command line:**

```{bash}
Red -gnm . -msk masked -cor ${NCPUS}
```

**Arguments:**

- `-gnm`: Specifies the location or path to the genome.
- `-msk`: Masking step.
- `-cor`:Specifies the number of CPU.

**Variables:**

- NCPUS: Number of CPU

### 5. Evidences: Mapping

Hisat2 is used for mapping the reads and Samtools to sort and index them.

#### **a) Hisat2**

`bin/hisat2.sh` shell script that is called in the `modules/hisat2.nf`module needed in the workflow.

Version of the Singularity image : _**hisat2:2.2.1--hdbdd923_6**_

##### **Genome indexing:**

**Command line:**

```{bash}
hisat2-build -p ${NCPUS} ${FASTA} ${FASTA%.*}
```
**Arguments:**

- `p`: Specifies the number of CPU.

**Variables:**

- NCPUS: Number of CPU
- FASTA: Sequencing files(fastq.gz)

##### **Reads mapping:**

**Command line:**

```{bash}
hisat2 -p ${NCPUS} --no-unal -q -x $INDEX -1 ${R1} -2 ${R2} --max-intronlen ${INTRON} > ${SAM}
```
**Arguments:**

- `-p`: Specifies the number of CPU .
- `--no-unal`: Disables reporting of unaligned reads in the output.
- `-q`: Assumes that the input files are in FASTQ format.
- `-x`: Specifies the path to the index files generated by HISAT2.
- `-1`: Specifies the paths to the forward paired-end input read files .
- `-2`: Specifies the paths to the backward paired-end input read files .
- `--max-intronlen`: Sets the maximum intron length.

**Variables:**

- NCPUS: Number of CPU
- INDEX: Index files' path
- R1: Fasta files containing forward reads 
- R2: Fasta files containing backward reads 
- INTRON: Maximum intron size 
- SAM: .sam output files 

#### **b) Samtools**

`bin/samtool.sh` shell script that is called in the `modules/samtools.nf` module needed in the workflow.

Version of the Singularity image : _**samtools:1.19.2--h50ea8bc_0**_

##### **Reads sorting:**

**Command line:**

```{bash}
samtools sort -o ${BAM} -@ ${NCPUS} -m 2G -O bam -T tmp ${SAM}
```
**Arguments:**

- `-o`: Specifies the output BAM file.
- `-@`: Specifies the number of CPU.
- `-m`: Sets the maximum memory per thread.
- `-O`: Specifies the output format.
- `-T`: Specifies the prefix for temporary files created during the sorting process. 

**Variables:**
- BAM: Output bam file 
- NCPUS: Number of CPU
- SAM: .sam tempporary files

##### **Reads indexing:**

**Command line:**

```{bash}
samtools index ${BAM}
```

**Variables:**
- BAM: .bam files containing sorted reads.

### 6. Annotation

Annotation of the protein coding genes using BRAKER3.

`bin/brakers.sh` shell script that is called in the `modules/brakers.nf`module needed in the workflow.

Version of the Singularity image : _**braker3:v3.0.7.1**_

**Command line:**

```{bash}
braker.pl --species=${SPECIES} --genome=${MSK_FASTA} --bam=${RNASEQ} --prot_seq=${PROT} \
          --workingdir=braker3 --gff3 --threads ${NCPUS}
```
**Arguments:**

- `--species`: Specifies the species for which gene prediction is being performed. 
- `--genome`: Specifies the path to the genome file in FASTA format. 
-` --bam`: Specifies the path to the RNA-Seq alignment file in BAM format.
- `--prot_seq`: Specifies the path to protein sequences in FASTA format. 
- `--workingdir`: Specifies the working directory where BRAKER will store its intermediate and output files. 
- `--gff3`: Specifies that the output annotation file should be in GFF3 format.
- `--threads`: Specifies the number of CPU.

**Variables:**

- SPECIES: Dunaliella_primolecta
- MSK_FASTA: Redmask output
- RNASEQ: .bam rna-seq alignment files
- PROT: Protein sequences of chlamydomonadales
- NCPUS: Number of CPU

## Input data

The workflow works on Pacbio and Illumina sequencing files with .fastq.gz extensions.

The data of the study can be found here : 
https://portal.darwintreeoflife.org/data/root/details/Dunaliella%20primolecta

## Install and run 

### Compute requirement

Recommended requirements: 

- OS: Unix
- CPUs: 2
- Memory: 16G 

Approximate run time: 9 hours


### Install

To run the following workflow, the user must first clone this repository on his local machine or on a cluster with the following command line:

```{bash}
git clone git@gitlab.com:magiclex/nf_assemblify-base.git
```

### Run the pipeline

The workflow is aimed to be run on a cluster with the run_assemblify file. If you work from a cluster, in order to run this workflow you have to tap on your terminal:

```{bash}
sbatch run_assemblify.slurm
```

If you work from your local device then you have to first install Nextflow version **23.04.1**.

Then,  tap the following command lines:

```{bash}
module load nextflow
nextflow run main.nf --profile Singularity
```

In the terminal type the following command line:

```{bash}
sbatch run_assemblify.slurm
```

In order to monitor your job, type the following command line: 

`squeue -u $USER`

## License

**CC0 1.0 Universal** ![CC0 1.0 universel](https://img.shields.io/badge/License%20-%20CC0%201.0%20Universal%20-%20red?style=flat-square&logoColor=black)

## Citation

Lerévérend, A., & Kardous, I. (2024). Nextflow Assemblify Workflow from PLA course in Master 2 BioInformatics from Rennes (Version 1). Zenodo. https://doi.org/10.5281/zenodo.10608880

## Authors

Inès Kardous (MSc Student in Bioinformatics, University Rennes1)

ines.kardous@etudiant.univ-rennes.fr

Alexandre Lerévérend (MSc Student in Bioinformatics, University Rennes1)

alexandre.lereverend@etudiant.univ-rennes.fr

