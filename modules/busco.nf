process busco {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'midmem'

    // tag is used to display task name in Nextflow logs
    // Here we use input channel file name
    tag "busco_${assembly_fa.baseName}"
//check if need to modify!

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/02b_busco",        mode: 'copy', pattern: '*.busco'
    publishDir "${params.resultdir}/logs/busco",       mode: 'copy', pattern: 'busco*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: 'busco*.cmd'

    // Workflow input stream. Since this stream can be a list of fastq files,
    input:
        path(assembly_fa)
        path(busco_path)
        val(busco_dbName)

    
    // Workflow output stream
    output:
        // Different results
        path("*.busco")
        // we also collect commands executed and log files
        path("busco*.log")
        path("busco*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    busco.sh ${task.cpus} ${assembly_fa} ${busco_path} ${busco_dbName}  busco.cmd >& busco.log 2>&1
    """
}
