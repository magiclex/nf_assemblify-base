process hifiasm {

    label 'midmem'

    tag "hifiasm"

    publishDir "${params.resultdir}/02a_hifiasm",	mode: 'copy', pattern: '*.bp.p_ctg.fa'
    publishDir "${params.resultdir}/logs/hifiasm",	mode: 'copy', pattern: 'hifi*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'hifi*.cmd'

    input:
        path(qc_ch)

    output:
        path("*.bp.p_ctg.fa"), emit: assembly_fa
        path("hifi*.log")
        path("hifi*.cmd")

    script:
    """
    hifiasm.sh $qc_ch ${task.cpus} hifiasm.cmd >& hifiasm.log 2>&1
    """ 
}



