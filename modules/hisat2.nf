process hisat2 {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'highmem'

    // tag is used to display task name in Nextflow logs
    tag "hisat2"

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/04a_hisat2",        mode: 'copy', pattern: '*.ht2'
    publishDir "${params.resultdir}/04a_hisat2",        mode: 'copy', pattern: '*.sam'
    publishDir "${params.resultdir}/logs/hisat2",       mode: 'copy', pattern: 'hisat2*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'hisat2*.cmd'

    // Workflow input stream.
    input:
        path(redmask_ch)
        path(illumina_r1_ch)
        path(illumina_r2_ch)
    
    // Workflow output stream
    output:
        path("*.ht2")
        path("*.sam"), emit: sam_ch
        path("*.log"), emit: log_ch
        path("*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    hisat2.sh ${task.cpus} ${redmask_ch} ${illumina_r1_ch} ${illumina_r2_ch} hisat2.cmd >& hisat2.log 2>&1
    """
}
