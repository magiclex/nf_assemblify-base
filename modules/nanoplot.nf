process nanoplot {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'lowmem'

    // tag is used to display task name in Nextflow logs
    // Here we use input channel file name
    tag "nanoplot_${fq_ch.baseName}"

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/01c_nanoplot",        mode: 'copy', pattern: '*.html'
    publishDir "${params.resultdir}/01c_nanoplot",        mode: 'copy', pattern: '*.png'
    publishDir "${params.resultdir}/01c_nanoplot",        mode: 'copy', pattern: '*.txt'
    publishDir "${params.resultdir}/logs/nanoplot",       mode: 'copy', pattern: 'nanoplot*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: 'nanoplot*.cmd'

    // Workflow input stream. Since this stream can be a list of fastq files,
    // we use 'each()' to let Nextflow run this process on each file.
    input:
        each(fq_ch)

    // Workflow output stream
    output:
        // Different results
        path("*.*")
        // we also collect commands executed and log files
        path("nanoplot*.log")
        path("nanoplot*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    nanoplot.sh ${task.cpus} $fq_ch nanoplot.cmd >& nanoplot.log 2>&1
    """
}
