process redmask {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'midmem'

    // tag is used to display task name in Nextflow logs
    tag "redmask"

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/03_redmask",        mode: 'copy', pattern: 'masked/*.msk'
    publishDir "${params.resultdir}/logs/redmask",       mode: 'copy', pattern: 'redmask*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'redmask*.cmd'

    // Workflow input stream.
    input:
        path(assembly_fa)

    
    // Workflow output stream
    output:
        path("masked/*.msk"), emit: redmask_ch
        path("redmask*.log")
        path("redmask*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    redmask.sh ${assembly_fa} ${task.cpus} redmask.cmd >& redmask.log 2>&1
    """
}
