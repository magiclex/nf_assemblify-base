process braker3 {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'highmem'

    // tag is used to display task name in Nextflow logs
    tag "braker3"

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/05a_braker3",        mode: 'copy', pattern: 'braker3/*.gff3'
    publishDir "${params.resultdir}/05a_braker3",        mode: 'copy', pattern: 'braker3/*.aa'
    publishDir "${params.resultdir}/logs/braker3",       mode: 'copy', pattern: 'braker3/*.log'
    publishDir "${params.resultdir}/logs/braker3",       mode: 'copy', pattern: 'braker3*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'braker3*.cmd'

    // Workflow input stream.
    input:
        val(species)
        path(red_ch)
        path(rnaseq)
        path(prot)

    
    // Workflow output stream
    output:
        path("braker3/*.gff3"), emit: gff3_ch
        path("braker3/*.aa"), emit: faa_ch
        path("braker3/*.log")
        path("braker3*.log")
        path("braker3*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    braker3.sh ${species} ${red_ch} ${rnaseq} ${prot} ${task.cpus} braker3.cmd >& braker3.log 2>&1
    """
}
