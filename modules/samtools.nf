process samtools {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'highmem'

    // tag is used to display task name in Nextflow logs
    tag "samtools_${sam_ch.baseName}"

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/04b_samtools",        mode: 'copy', pattern: '*.bam'
    publishDir "${params.resultdir}/04b_samtools",        mode: 'copy', pattern: '*.bai'
    publishDir "${params.resultdir}/logs/samtools",       mode: 'copy', pattern: 'samtools*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",  mode: 'copy', pattern: 'samtools*.cmd'

    // Workflow input stream.
    input:
        each(sam_ch)

    
    // Workflow output stream
    output:
        path("*.bam"), emit: bam_ch
        path("*.bai")
        path("*.log")
        path("*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    samtools.sh ${task.cpus} ${sam_ch} samtools_${sam_ch.baseName}.cmd >& samtools_${sam_ch.baseName}.log 2>&1
    """
}
