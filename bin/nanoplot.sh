#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                               Nanoplot                                    ##
##                                                                           ##
###############################################################################

# Get script arguments coming from modules/nanoplot.nf process
args=("$@")
FASTQ_FILE=${args[1]}
LOGCMD=${args[2]}
NCPUS=${args[0]}
OUTDIR=${args[3]}

# Command to execute
CMD="NanoPlot -t ${NCPUS} --plots kde dot hex --N50 --title \"PacBio Hifi reads for $(basename ${FASTQ_FILE%.hifi*})\" --fastq ${FASTQ_FILE} -o ."

# Save command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}
