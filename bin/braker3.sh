#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                        Prediction with Braker3                            ##
##                                                                           ##
###############################################################################

# Get script arguments coming from modules/braker3.nf process
args=("$@")
SPECIES=${args[0]}
MSK_FASTA=${args[1]}
RNASEQ=${args[2]}
PROT=${args[3]}
NCPUS=${args[4]}
LOGCMD=${args[5]}


# Command to execute (varaiable masked not created yet, the output directory)
CMD="braker.pl --species=${SPECIES} --genome=${MSK_FASTA} --bam=${RNASEQ} --prot_seq=${PROT} \
          --workingdir=braker3 --gff3 --threads ${NCPUS}"

# Save command in log
echo ${CMD} > ${LOGCMD}

eval ${CMD}
