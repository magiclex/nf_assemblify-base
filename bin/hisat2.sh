#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                                  Hisat2                                   ##
##                                                                           ##
###############################################################################

# Get script arguments coming from modules/hisat2.nf process
args=("$@")
NCPUS=${args[0]}
FASTA=${args[1]}
R1=${args[2]}
R2=${args[3]}
LOGCMD=${args[4]}

INTRON=20000
SAM=$(basename ${R1%.fastq*}).sam
INDEX=${FASTA%.*}

# Command to execute
CMD="hisat2-build -p ${NCPUS} ${FASTA} ${FASTA%.*} ; \
    hisat2 -p ${NCPUS} --no-unal -q -x $INDEX -1 ${R1} -2 ${R2} --max-intronlen ${INTRON} > ${SAM}"

# Save command in log
echo ${CMD} > ${LOGCMD}

eval ${CMD}