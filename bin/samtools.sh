#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##              Converting SAM to BAM with samtools, with indexing           ##
##                                                                           ##
###############################################################################

# Get script arguments coming from modules/samtools.nf process
args=("$@")
NCPUS=${args[0]}
SAM=${args[1]}
LOGCMD=${args[2]}

BAM=$(basename ${SAM%.*}).bam

# Command to execute
CMD1="samtools sort -o ${BAM} -@ ${NCPUS} -m 2G -O bam -T tmp ${SAM}"
#should be possible to get the 2G from the parameters
CMD2="samtools index ${BAM}"

# Save command in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}

eval ${CMD1}
eval ${CMD2}