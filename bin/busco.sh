#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                      Check assembly using Busco                           ##
##                                                                           ##
###############################################################################

# Get script arguments coming from modules/busco.nf process
args=("$@")
ASSEMBLY_FA=${args[1]}
LOGCMD=${args[4]}
NCPUS=${args[0]}
DBPATH=${args[2]}
DBNAME=${args[3]}

#RES_FILE = $(basename ${ASSEMBLY_FILE%.*}).busco
#CMD="busco -c ${NCPUS} --offline -m genome -i ${ASSEMBLY_FA} -o ${RES_FILE} -l ${DBPATH}/${DBNAME}" (2eme option, 1ere option est valable)

# Command to execute
CMD="busco -c ${NCPUS} --offline -m genome -i ${ASSEMBLY_FA} -o res.busco -l ${DBPATH}/${DBNAME}"

# Save command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}
