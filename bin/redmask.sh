#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##                           Repeat Detector Red                             ##
##                                                                           ##
###############################################################################

# Get script arguments coming from modules/redmask.nf process
args=("$@")
GENOMEDIR=${args[0]}
NCPUS=${args[1]}
LOGCMD=${args[2]}


# Command to execute (varaiable masked not created yet, the output directory)
CMD="Red -gnm . -msk masked -cor ${NCPUS}"

# Create a directory for the results (echo first)
echo "mkdir -p masked"

# Save command in log
echo ${CMD} > ${LOGCMD}

# Execute commands (-p for ignoring if directory already exists)
mkdir -p masked

eval ${CMD}
